
Module Based Design Pattern 

step 1: composer require nwidart/laravel-modules

step 2: php artisan vendor:publish --provider="Nwidart\Modules\LaravelModulesServiceProvider"

step 3: insert in composer.json

   {
  "autoload": {
    "psr-4": {
      "App\\": "app/",
      "Modules\\": "Modules/"
    }
  }
}

step 4: composer dump-autoload


For Form Validation 

step 1 : php artisan module:make-request StoreCsv Csv

step 2 : php artisan module:make-request UpdateCsv Csv

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
