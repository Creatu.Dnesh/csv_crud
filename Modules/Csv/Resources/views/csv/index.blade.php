@extends('csv::layouts.admin-master')

<!-- Title Goes Here -->
@section('content')

@include('csv::message.message')
<div class="login-root mt-5">
	<div class="box-root flex-flex flex-direction--column" style="min-height: 100vh;flex-grow: 1;">
		<div class="box-root padding-top--24 flex-flex flex-direction--column" style="flex-grow: 1; z-index: 9;">
			<div class="container">
				<div class="rightbar mb-4">
					<h4 class="text-center mb-4">View Register Form</h4>
					<div class="form-row">
						<div class="form-group col-md-2 text-center">
							<a href="{{url('/csv/form')}}"><button class="submit btn"><i class="fa fa-plus"></i> Add</button></a>
						</div>
					</div>
					<div id="Div1">
						<div class="table-responsive">
							<table class="table action-table">
								<thead class="thead-light">
									<tr>
										<th scope="col">SN</th>
										<th scope="col">Name</th>
										<th scope="col">Email</th>
										<th scope="col">Address</th>
										<th scope="col">Phone No</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($getData as $key => $view)
								
								@if(count($view) > 1)
									<tr>
										<td>{{$view['0']}}</td>
										<td>{{$view['1']}}</td>
										<td>{{$view['2']}}</td>
										<td>{{$view['3']}}</td>
										<td>{{$view['4']}}</td>
										<td>
												
												<a href="{{url('csv/'.$key.'/show')}}" class="btn btn-success">
													Show
												</a>
												<a href="{{url('csv/'.$key.'/edit')}}" class="btn btn-success">
													Edit
												</a>
											
												<a href="{{url('csv/'.$key.'/delete')}}" onclick="return confirm('Are you sure?')" class="btn btn-danger">
													Delete
												</a>
										</td>
									</tr>
								@endif
								@endforeach								
								</tbody>
							</table>
						</div>
					</div>  
				</div>
			</div>
		</div>
	</div>
</div>

@endsection