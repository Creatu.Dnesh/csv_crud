@extends('csv::layouts.admin-master')

@section('content')

@include('csv::message.message')
<div class="login-root">
	<div class="box-root flex-flex flex-direction--column" style="min-height: 100vh;flex-grow: 1;">
		<div class="box-root padding-top--24 flex-flex flex-direction--column" style="flex-grow: 1; z-index: 9;">
			<div class="row">
				<div class="offset-md-2 col-md-8">
					<div class="formbg-outer">
						<div class="formbg">
							<div class="formbg-inner padding-horizontal--48">
								<h4 class="text-center mb-4">Edit Register Form</h4>
								<div class="form-row">
									<div class="form-group col-md-6 ">
										<a href="{{url('/csv/')}}"><button class="submit btn btn-success"><i class="fa fa-right-angle"></i> Back</button></a>
									</div>
								</div>
								<hr>
								<form id="stripe-login" method="post" action="{{url('/csv/'.$getData[0].'/update')}}">
									@csrf
									<div class="form-row mt-4">
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="name">Name</label>
											<input type="text" name="name" id="name" value="{{ $getData[1] }}">
											@if ($errors->has('name')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('name')}}</span>
											@endif
										</div>

										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="gender">Gender</label>
											<select id="gender" class="form-control" id="gender" name="gender">

												<option disabled="" selected>Select Any One</option>

												<option @if($getData[7] == 'Male') selected="" @endif name="male">Male</option>

												<option @if($getData[7] == 'Female') selected="" @endif name="female">Female</option>

												<option @if($getData[7] == 'Other') selected="" @endif name="other">Other</option>
											</select>
											@if ($errors->has('gender')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('gender')}}</span>
											@endif
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="email">Email</label>
											<input type="email" name="email" id="email" value="{{ $getData[2] }}">
											@if ($errors->has('email')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('email')}}</span>
											@endif
										</div>
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="address">Address</label>
											<input type="text" name="address" id="address" value="{{ $getData[3] }}">
											@if ($errors->has('address')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('address')}}</span>
											@endif
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="nationality">Nationality</label>
											<input type="text" name="nationality" id="nationality" value="{{ $getData[6] }}">
											@if ($errors->has('nationality')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('nationality')}}</span>
											@endif
										</div>
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="date">Date Of Birth</label>
											<input type="date" name="dob" id="dob" value="{{$getData[5]}}">
											@if ($errors->has('dob')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('dob')}}</span>
											@endif
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="date">Phone Number</label>
											<input type="number" name="phone" id="" placeholder="Please enter your Phone number" value="{{ $getData[4] }}">
											@if ($errors->has('phone')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('phone')}}</span>
											@endif
										</div>
										
										<div class="form-group col-md-6 field padding-bottom--24">
											<label for="inputState">Mode of Contact</label>
											<select id="inputState" class="form-control" name="mode">
												<option disabled="" selected>Select Any One</option>
												<option @if($getData[8] == 'email') selected="" @endif value="email">Email</option>
												<option @if($getData[8] == 'phone') selected="" @endif  value="phone">Phone</option>
												<option @if($getData[8] == 'none') selected="" @endif value="none">None</option>
											</select>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12 field padding-bottom--24">
											<label for="education">Education Detail</label>
											<textarea class="form-control" type="text" rows="5" name="education" id="education">{{$getData[9]}}</textarea>
											@if ($errors->has('education')) 
											<span style="color: red; font-size: 15px;">{{$errors->first('education')}}</span>
											@endif
										</div>
									</div>

									<div class="form-row">
										<div class="form-group col-md-12 text-center field padding-bottom--24">
											<button class="submit btn" type="submit" value="Continue">Update</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection