@extends('csv::layouts.admin-master')

@section('content')

@include('csv::message.message')
<div class="login-root mt-5">
	<div class="box-root flex-flex flex-direction--column" style="min-height: 100vh;flex-grow: 1;">
		<div class="box-root padding-top--24 flex-flex flex-direction--column" style="flex-grow: 1; z-index: 9;">
			<div class="container">

				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-header">
								
								<h4 class="text-center mb-4">Detail View</h4>
								<div class="form-row">
									<div class="form-group col-md-6 ">
										<a href="{{url('/csv/')}}"><button class="submit btn btn-success"><i class="fa fa-right-angle"></i> Back</button></a>
									</div>
									<div class="form-group col-md-6 text-right">
										<a href="{{url('/csv/'.$getData[0].'/edit')}}"><button class="submit btn btn-primary"><i class="fa fa-pencil"></i> Edit</button></a>
									</div>
								</div>

							</div>
							<hr>
							<div class="card-body">
								<div class="row">
									<div class="col-12 col-sm-9 col-md-3 col-lg-3">
										<div class="users-view-image" style="width: 100%">
											<img src="{{url('/image/profile.png')}}" 
											onerror="{{url('/image/profile.png')}}"
											class="users-avatar-shadow w-100 mb-2 pr-2 ml-1" alt="avatar">
										</div>
									</div>
									<div class="col-12 col-sm-9 col-md-3 col-lg-5">
										<table>
											<tr>
												<td class="font-weight-bold">Name :- </td>
												<td>{{$getData[1]}}</td>
											</tr>
											<tr>
												<td class="font-weight-bold">Email :- </td>
												<td>{{$getData[2]}}</td>
											</tr>
											<tr>
												<td class="font-weight-bold">Nationality :- </td>
												<td>{{$getData[6]}}</td>
											</tr>
											<tr>
												<td class="font-weight-bold">Address :- </td>
												<td>{{$getData[3]}}</td>
											</tr>
											<tr>
												<td class="font-weight-bold">Contact :- </td>
												<td>{{$getData[4]}}</td>
											</tr>
											<tr>
												<td class="font-weight-bold">Mode Of Contact :-  </td>
												<td>
													<span class="badge badge-success">{{$getData[8]}}</span>
												</td>
											</tr>
											<tr>
												<td class="font-weight-bold">DOB :- </td>
												<td>{{$getData[5]}}</td>
											</tr>
											
											<tr>
												<td class="font-weight-bold">Education Detail :- </td>
												<td>
													{{$getData[9]}}
												</td>
											</tr>
										</table>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
