<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page Title -->
    <title>Register</title>


    <!-- FavIcon Link -->
    <link rel="icon" type="image/ico" href="{{url('favicon.ico')}}">


    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="description" content="">
    <meta name="keywords" content="">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">

    <!-- fontawesome css -->

    <!-- Global CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <!-- Jquery -->

    <!-- Jquery 1.9.1 -->
    <script type="text/javascript" src="{{url('js/jquery-1.9.1.min.js')}}"></script>



</head>

<body>

    <div>
        <!-- header -->
    </div>

    <div style="min-height: 50vh">
       @yield('content')
   </div>

   <div>
       @include('csv::layouts.footer.footer')
   </div>

</body>


<script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
<!-- Global JS -->
<script type="text/javascript" src="{{url('js/js.js')}}"></script>

<!-- Main js -->
@yield('js')
<!-- /.js -->
</html>