<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('csv')->group(function() {

	Route::get('/','CsvController@index')->name('csv.index');
	Route::get('/form', 'CsvController@create')->name('csv.create');
	Route::post('submit_form','CsvController@store')->name('csv.store');
	Route::get('{key}/show','CsvController@show')->name('csv.show');
	Route::get('{key}/edit','CsvController@edit')->name('csv.edit');
	Route::post('{key}/update','CsvController@update')->name('csv.update');
	Route::get('{key}/delete','CsvController@destroy')->name('csv.destroy');

});
