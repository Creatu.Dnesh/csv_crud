<?php

namespace Modules\Csv\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCsv extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>'required|email|max:255',
            'name' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'phone' => 'required|min:10|max:10',
            'nationality' => 'required',
            'dob' => 'required',
            'mode' => 'required',
            'education' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
