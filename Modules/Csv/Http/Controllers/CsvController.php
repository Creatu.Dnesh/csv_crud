<?php

namespace Modules\Csv\Http\Controllers;

use Modules\Csv\Http\Repository\Eloquent\CsvRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Csv\Http\traits\Filterstring;
use Illuminate\Routing\Controller;
use Modules\Csv\Http\Requests\StoreCsv;
use Modules\Csv\Http\Requests\UpdateCsv;

class CsvController extends Controller
{
    use Filterstring ;

    private $CsvRepository;

    public function __construct(CsvRepository $CsvRepository)
    {
       $this->CsvRepository = $CsvRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try{
            $getData = $this->CsvRepository->all();
            return view('csv::csv.index',compact('getData'));
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('csv::csv.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreCsv $request)
    {
        try{
            $data['name'] = $this->clean_text($request["name"]);
            $data['email'] = $this->clean_text($request["email"]);
            $data['address'] = $this->clean_text($request["address"]);
            $data['phone'] = $this->clean_text($request["phone"]);
            $data['dob'] = $this->clean_text($request["dob"]);
            $data['nationality'] = $this->clean_text($request["nationality"]);
            $data['gender'] = $this->clean_text($request["gender"]);
            $data['mode'] = $this->clean_text($request["mode"]);
            $data['education'] = $this->clean_text($request["education"]);
            $this->CsvRepository->create($data);
            return redirect()->to('/csv')->with('success','Data Stored Successfully');;
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try{
            $getData = $this->CsvRepository->find($id);
            return view('csv::csv.show',compact('getData'));
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        try{
            $getData = $this->CsvRepository->find($id);
            return view('csv::csv.edit',compact('getData'));
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateCsv $request, $id)
    {
        try{
            $data['name'] = $this->clean_text($request["name"]);
            $data['email'] = $this->clean_text($request["email"]);
            $data['address'] = $this->clean_text($request["address"]);
            $data['phone'] = $this->clean_text($request["phone"]);
            $data['dob'] = $this->clean_text($request["dob"]);
            $data['nationality'] = $this->clean_text($request["nationality"]);
            $data['gender'] = $this->clean_text($request["gender"]);
            $data['mode'] = $this->clean_text($request["mode"]);
            $data['education'] = $this->clean_text($request["education"]);
            $getData = $this->CsvRepository->update($data,$id);
            return redirect()->to('/csv')->with('success','Data updated Successfully');
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $getData = $this->CsvRepository->delete($id);
            return redirect()->to('/csv')->with('success','Data Deleted Successfully');;
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }
}
