<?php

namespace Modules\Csv\Http\traits;

use Illuminate\Http\Request;

trait Filterstring
{
	function clean_text($string)
	{
	 	$string = trim($string);
	 	$string = stripslashes($string);
	 	$string = htmlspecialchars($string);
	 	return $string;
	}
}