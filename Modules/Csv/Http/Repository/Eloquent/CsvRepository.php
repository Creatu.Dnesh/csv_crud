<?php

namespace Modules\Csv\Http\Repository\Eloquent;

use Modules\Csv\Http\Repository\CsvRepositoryInterface;

use Illuminate\Support\Collection;

Use Illuminate\Database\QueryException;

class CsvRepository implements CsvRepositoryInterface
{

    public function create(array $attributes)
    {
        try {

            if (!file_exists(public_path('/uploads')))
            {
                mkdir(public_path('/uploads'), 755, true);
            }

            $file_open = fopen(public_path("uploads/contact_data.csv"), "a");
            $no_rows = count(file(public_path("uploads/contact_data.csv")));

            if($no_rows > 1)
            {
                $no_rows = ($no_rows - 1) + 1;
            }

            $form_data = array(
               'sr_no'  => $no_rows,
               'name'  => $attributes['name'],
               'email'  => $attributes['email'],
               'address' => $attributes['address'],
               'phone' => $attributes['phone'],
               'dob' => $attributes['dob'],
               'nationality' => $attributes['nationality'],
               'gender' => $attributes['gender'],
               'mode' => $attributes['mode'],
               'education' => $attributes['education']
              );

            fputcsv($file_open, $form_data);
            return $form_data;
        } 
        catch (\Exception $e) 
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }
 
    /**
    * @param $id
    * @return success
    */
    public function find($id)
    {
        $cnt = 0;
        $getData = [];

        try{
            if (($handle = fopen(public_path("uploads/contact_data.csv"), "r")) !== FALSE) 
            {
                while (($csvadata = fgetcsv($handle, 0, ",")) !== FALSE) 
                {      
                   $data[$cnt++] = $csvadata;
                }
               
                fclose($handle);
                $getData = $data[$id];
                return $getData; 
            }
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }


    /**
    * @param array $attributes
    * @param $id
    * @return success
    */
    public function update(array $attributes,$id)
    {
        try {

            $cnt = 0;
            $getData = [];
            if (($handle = fopen(public_path("uploads/contact_data.csv"), "r")) !== FALSE) 
            {
                while (($csvadata = fgetcsv($handle, 0, ",")) !== FALSE) 
                {      
                   $data[$cnt++] = $csvadata;
                }

               if(!empty($data[$id]))
                {
                    $data[$id][1] = $attributes['name'];
                    $data[$id][2] = $attributes['email'];
                    $data[$id][3] = $attributes['address'];
                    $data[$id][4] = $attributes['phone'];
                    $data[$id][5] = $attributes['dob'];
                    $data[$id][6] = $attributes['nationality'];
                    $data[$id][7] = $attributes['gender'];
                    $data[$id][8] = $attributes['mode'];
                    $data[$id][9] = $attributes['education'];
                }
                fclose($handle);  
            }

            $fp = fopen(public_path('uploads/contact_data.csv'), 'w');
            foreach ($data as $fields) {

                fputcsv($fp, $fields);
            }

            fclose($fp);
            return $fp;
            
        } 
        catch (\Exception $e) 
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
    }
   /**
    * @return data
    */
   public function all()
   {
        $data = [];
        $getData = [];

        try {
            $CSVfp = fopen("uploads/contact_data.csv", "r");
            if($CSVfp !== FALSE) 
            {
                while(! feof($CSVfp)) 
                {
                    $data = fgetcsv($CSVfp, 1000, ",");
                    if(!empty($data)){
                        $getData[count($getData)] = $data;
                    }
                    
                }
                fclose($CSVfp);
                return $getData;
            }
        }
        catch (\Exception $e) 
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }
          
   }

   /**
    * @return success
    */
   public function delete($id)
   {
        try{
            $cnt = 0;
            $getData = [];
            if (($handle = fopen(public_path("uploads/contact_data.csv"), "r")) !== FALSE) 
            {
                while (($csvadata = fgetcsv($handle, 0, ",")) !== FALSE) 
                {      
                   $data[$cnt++] = $csvadata;
                }
               
                fclose($handle);
                unset($data[$id]);
                
            }

            $fp = fopen(public_path('uploads/contact_data.csv'), 'w');
            foreach ($data as $fields) {

                fputcsv($fp, $fields);
            }

            fclose($fp);
            return $fp; 
        }
        catch(\Exception $e)
        {
            $exception = $e->getMessage();
            return redirect()->back()->with('error',$exception);
        }   
   }
 }