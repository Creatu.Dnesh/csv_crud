<?php
namespace Modules\Csv\Http\Repository;

use Illuminate\Support\Collection;

interface CsvRepositoryInterface
{

   public function all();

   public function create(array $attributes);

   public function find($id);

   public function update(array $attributes,$id);

   public function delete($key);
}